// Start bài số chẵn lẻ
function soChanLe() {
  var soChan = "";
  var soLe = "";

  for (var step = 0; step < 100; step++) {
    if (step % 2 == 0) {
      soChan = soChan + step + " ";
    } else {
      soLe = soLe + step + " ";
    }
  }
  document.getElementById(
    "result-chan-le"
  ).innerHTML = `Số chẵn: ${soChan} <br> Số lẻ: ${soLe}`;
}
// End bài số chẵn lẻ

// Start bài 1
function soNguyenDuong() {
  var num = 0;
  var sumNumber = 0;
  while (sumNumber < 10000) {
    num++;
    sumNumber += num;
  }
  document.getElementById(
    "result1"
  ).innerHTML = `Số nguyên dương nhỏ nhất: ${num}`;
}
// End bài 1

// Start bài 2
function tinhTong() {
  const x = Number(document.getElementById("so-x").value);
  const n = Number(document.getElementById("so-n").value);
  var sumTotal = 0;
  var i = 1;
  while (i <= n) {
    sumTotal += Math.pow(x, i);
    i++;
  }
  document.getElementById("result2").innerHTML = `Tổng: ${sumTotal}`;
}
// End bài 2

// Start bài 3
function tinhGiaiThua() {
  const giaiThua = Number(document.getElementById("n-giai-thua").value);
  var tichGiaiThua = 1;
  var i = 2;
  for (i; i <= giaiThua; i++) {
    tichGiaiThua *= i;
  }
  document.getElementById("result3").innerHTML = `Giai thừa: ${tichGiaiThua}`;
}
// End bài 3

// Start bài 4
function taoThe() {
  var background = "";
  var i = 1;
  for (i; i <= 10; i++) {
    if (i % 2 == 0) {
      background +=
        '<div class="bg-danger" style="color: white">div chẵn</div>';
    } else {
      background += '<div class="bg-primary" style="color: white">div lẻ</div>';
    }
  }
  document.getElementById("result4").innerHTML = background;
}
// End bài 4

// Start bài 5
function checkNguyenTo(nguyento) {
  var checkNguyenTo = Math.sqrt(nguyento);
  var i = 2;
  for (i; i <= checkNguyenTo; i++) {
    if (nguyento % i === 0) {
      return;
    }
  }
  return 1;
}

function checkSoNguyenTo() {
  const soNguyenTo = Number(document.getElementById("n-nguyen-to").value);
  var nguyenToArray = [];
  // Số nguyên tố là số chia dư 1
  // Nếu = 2 hoặc chia = 0 thì số đó không phải số nguyên tố
  if (soNguyenTo < 2) {
    alert(`Nhập số lớn hơn 1`);
  }

  var i = 2;
  for (i; i <= soNguyenTo; i++) {
    if (checkNguyenTo(i) === 1) {
      // Push vào mảng nếu chia ra dư 1
      // Đúng điều kiện của số nguyên tố
      nguyenToArray.push(i);
    }
  }
  document.getElementById("result5").innerHTML = nguyenToArray;
}
// End bài 5
